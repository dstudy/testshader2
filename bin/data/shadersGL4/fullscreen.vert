//basic
#version 400

const vec2 madd = vec2(0.5, 0.5);

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 texcoord;

out vec2 UV;

uniform mat4 ModelViewMatrix;
// uniform mat3 NormalMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 MVP; // 投影 * モデルビュー


void main()
{
	UV = position.xy*madd+madd;
	gl_Position = vec4(position.xy, 0.0, 1.0);
}