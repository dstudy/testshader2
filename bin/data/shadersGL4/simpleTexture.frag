#version 400

in vec3 Pos;
// in vec3 Nor;
in vec2 UV;

uniform sampler2D RenderTex;

layout (location = 0) out vec4 FragColor;

void main() {
	vec4 texColor = texture(RenderTex, UV);
	FragColor = vec4(1.0, 1.0, 1.0, 1.0) * texColor;
}
