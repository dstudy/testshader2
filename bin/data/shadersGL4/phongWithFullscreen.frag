#version 400

#define PI (3.1415926536)
#define TWO_PI (6.2831853072)

in vec2 UV;
in vec3 LightIntensity;
in vec4 ColorOut;

uniform sampler2D RenderTex;
uniform float elapsedTime;
float time = elapsedTime;

layout (location = 0) out vec4 FragColor;


void main() {
	vec4 texColor = texture(RenderTex, UV);
	FragColor = texColor * vec4(ColorOut.xyz, 1.0);
}
