#version 400

const vec2 madd = vec2(0.5, 0.5);

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 texcoord;

out vec2 UV;
out vec3 LightIntensity;
out vec4 ColorOut;

uniform mat4 ModelViewMatrix;
uniform mat4 NormalMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 MVP; // 投影 * モデルビュー

uniform vec3 LightPosition;
uniform vec3 Kd;
uniform vec3 Ld;


void main()
{

	vec3 vertexnormal = (NormalMatrix * vec4(normal, 0.0)).xyz;
	vec3 tnorm = normalize(vertexnormal);
	vec4 eyeCoords = ModelViewMatrix * vec4(position, 1.0);
	vec3 s = normalize(vec3(LightPosition - eyeCoords));
	LightIntensity = Ld * Kd * max(dot(s, tnorm), 0.0);

	LightIntensity = vec3(normalize(distance(position, LightPosition)));


	float intensity = max(dot(vertexnormal, LightPosition), 0.0);

	ColorOut = vec4(1, 1, 1, 1) * normalize(intensity);


	UV = position.xy*madd+madd;
	gl_Position = vec4(position, 1.0);
	
	// UV = texcoord;
	// gl_Position = MVP * vec4(position, 1.0);
}