#version 400

#define PI (3.1415926536)
#define TWO_PI (6.2831853072)



in vec3 Pos;
// in vec3 Nor;
in vec2 UV;

uniform sampler2D RenderTex;
uniform float elapsedTime;
float time = elapsedTime;

layout (location = 0) out vec4 FragColor;


void main() {
	vec4 texColor = texture(RenderTex, UV);
	FragColor = vec4(sin(mod(elapsedTime*2, PI)), 1.0, 1.0, 1.0) * texColor;
}
