//basic
#version 400

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 texcoord;

out vec3 Color;
out vec3 Position;
out vec3 Normal;
out vec2 UV;

uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 MVP; // 投影 * モデルビュー


void main()
{
	Color = vec3(1.0, 0, 0);
	UV = texcoord;
	gl_Position = MVP * vec4(position, 1.0);
	// gl_Position = MVP * vec4(texcoord.xy, 0, 1.0);
}