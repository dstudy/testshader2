#version 400

in vec3 Color;
in vec2 UV;

out vec4 FragColor;

uniform sampler2D renderdTexture;

void main()
{
    FragColor = vec4(Color, 1.0);
}