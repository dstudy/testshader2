#include "ofApp.h"
#include "ofxAssimpModelLoader.h"

ofxAssimpModelLoader model;
ofEasyCam cam;
ofShader shader;
ofShader simpleTexShader;
ofVboMesh vboMesh;

GLuint fboHandle;

ofFbo fbo;
ofImage image;
ofTexture tex;


//--------------------------------------------------------------
void ofApp::setup(){

	ofDisableArbTex();

	ofLogNotice() << glGetString(GL_VERSION);

	fbo.allocate(512, 512, GL_RGBA);

	model.loadModel("untitled.obj");
	vboMesh = model.getMesh(0);
	shader.load("shadersGL4/phongWithFullscreen");
	simpleTexShader.load("shadersGL4/simpleTexture");

	ofBackground(ofColor(80));

	light.setPosition(100, 100, 100);

	image.load("brick1.jpg");
	tex.loadData(image.getPixelsRef());
	//cam.setDistance(50);

}

//--------------------------------------------------------------
void ofApp::update(){
	light.setPosition(
		0,
		0,
		cos(ofGetElapsedTimef()*.2) * ofGetWidth()
    );
}

//--------------------------------------------------------------
void ofApp::draw(){

	ofEnableDepthTest();

	// モデルに色付け
	// フレームバッファオブジェクトにテクスチャとして書き込み
	fbo.begin();
	ofClear(255, 255, 255, 0);

	shader.begin();

	ofMatrix4x4 modelMatrix;
	modelMatrix = model.getModelMatrix();

	ofMatrix4x4 viewMatrix;
	viewMatrix = ofGetCurrentViewMatrix();

	ofMatrix4x4 projectionMatrix;
	projectionMatrix = cam.getProjectionMatrix();

	ofMatrix4x4 modelViewMatrix;
	modelViewMatrix = modelMatrix * viewMatrix;

	ofMatrix4x4 mvpMatrix;
	mvpMatrix = modelMatrix * viewMatrix * projectionMatrix;

	ofMatrix4x4 normalMatrix;
	normalMatrix = ofMatrix4x4::getTransposedOf(modelViewMatrix.getInverse());

	shader.setUniform1f("elapsedTime", ofGetElapsedTimef());
	shader.setUniformTexture("renderTex", tex, 0);
	shader.setUniformMatrix4f("ModelViewMatrix", modelViewMatrix);
	shader.setUniformMatrix4f("ProjectionMatrix", projectionMatrix);
	shader.setUniformMatrix4f("MVP", mvpMatrix);
	shader.setUniformMatrix4f("NormalMatrix", normalMatrix);
	shader.setUniform3f("Kd", ofVec3f(0.9f, 0.5f, 0.3f));
	shader.setUniform3f("LD", ofVec3f(1.0f, 1.0f, 1.0f));
	shader.setUniform3f("LightPosition", light.getPosition());

	vboMesh.draw();

	shader.end();


	fbo.end();
	//


	// 俯瞰
	cam.begin();

	ofDrawGrid(100, 10, true, true, true, true);

	ofEnableLighting();

	light.draw();
	light.enable();

	simpleTexShader.begin();

	//ofMatrix4x4 modelMatrix;
	modelMatrix = model.getModelMatrix();

	//ofMatrix4x4 viewMatrix;
	viewMatrix = ofGetCurrentViewMatrix();

	//ofMatrix4x4 projectionMatrix;
	projectionMatrix = cam.getProjectionMatrix();

	//ofMatrix4x4 modelViewMatrix;
	modelViewMatrix = modelMatrix * viewMatrix;

	//ofMatrix4x4 mvpMatrix;
	mvpMatrix = modelMatrix * viewMatrix * projectionMatrix;

	simpleTexShader.setUniformTexture("renderTex", fbo, 0);
	simpleTexShader.setUniformMatrix4f("ModelViewMatrix", modelViewMatrix);
	simpleTexShader.setUniformMatrix4f("ProjectionMatrix", projectionMatrix);
	simpleTexShader.setUniformMatrix4f("MVP", mvpMatrix);

	vboMesh.draw();

	simpleTexShader.end();
	

	ofDisableLighting();
	light.disable();

	cam.end();

	ofDisableDepthTest();

	ofSetColor(255);
	tex.draw(0, 0, 128, 128);
	fbo.draw(0, 128, 256, 256);
	ofDrawBitmapStringHighlight("Source texture", 0, 0 + 15);
	ofDrawBitmapStringHighlight("Frame Buffer Object", 0, 128 + 15);


}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
